ARG BASE_IMAGE=python:3-slim
FROM ${BASE_IMAGE} as builder
WORKDIR /code/

RUN apt-get -y update && apt-get install -y golang && \
    go install github.com/msoap/shell2http@latest && \
    cp $(go env GOPATH)/bin/shell2http .

FROM ${BASE_IMAGE}
WORKDIR /app
COPY --from=builder /code/shell2http .

RUN python -m pip install module-qc-tools \
              module-qc-database-tools \
              module-qc-analysis-tools

# COPY entrypoint.sh /entrypoint.sh

EXPOSE 8080
# ENTRYPOINT ["/entrypoint.sh"]
ENTRYPOINT ["/app/shell2http"]
CMD ["-form", "-cgi", \
     "/mqt/help", "mqt --help", \
     "/mqat/help", "mqat --help", \
     "/mqdbt/help", "mqdbt --help"]

