# itk-demo-mqt

## Getting started

Module qc tools for LLS as a microservice.

- build Docker image
- play with emulator
- extract YARR config from pdb

## Build

```
docker compose build
```

## Run

```
docker compose up
```

- navigate to http://localhost:8080
- profit

